package com.example.databaseconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseConnection {
//    declaring necessities.
    private String url;
    private Properties properties = new Properties();

//    constructor.
    public DatabaseConnection(String host, String databaseName, String username, String password) {
        this.url = "jdbc:mysql://" + host + "/" + databaseName;
        this.properties.setProperty("user", username);
        this.properties.setProperty("password", password);
    }

//    create getconnection
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(this.url, properties);
    }
}
