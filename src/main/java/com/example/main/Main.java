package com.example.main;

import com.example.databaseconnection.DatabaseConnection;
import com.example.datamanipulation.*;
import com.example.dataoverview.ViewStudentListing;
import com.example.interfaces.ViewStudentListingInterface;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.swing.plaf.nimbus.State;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

@SpringBootApplication
public class Main {

	public static void main(String[] args) {
		// connection info.
		String host = "localhost";
		String databaseName = "defaultdatabase";
		String username = "user";
		String password = "Password_123";

		// create DatabaseConnection object.
		DatabaseConnection databaseConnection = new DatabaseConnection(host, databaseName, username, password);


		// try and catch errors to capture SQLerror exception.
		try {
			Connection connection = databaseConnection.getConnection();
			Statement statement = connection.createStatement();

			// call methods for options.
			Menu(statement);

		} catch(SQLException e) {
			System.out.println("Error: " + e);
		}
	}

	private static void Menu(Statement statement) {
		int choice;

		do {
			// menu or choices..
			Scanner sc = new Scanner(System.in);
			System.out.println("<=========== Student Information System Menu ================>");
			System.out.println("1] Add Student");
			System.out.println("2] Edit Student Information");
			System.out.println("3] Delete or Student");
			System.out.println("4] View Student Listing");
			System.out.println("0] Exit or quit program.");
			choice = sc.nextInt();

			switch (choice) {
				case 1:
					AddStudent addStudent = new AddStudent(statement);
					addStudent.InputCredentials();
					break;
				case 2:
					EditStudentInfo editStudentInfo = new EditStudentInfo(statement);
					editStudentInfo.EditWhichStudent();
					break;

				case 3:
					DeleteStudent deleteStudent = new DeleteStudent(statement);
					deleteStudent.DeleteWhichStudent();
					break;
				case 4:
					ViewStudentListing viewStudentListing = new ViewStudentListing(statement);
			}

		} while (choice != 0);
	}

}
