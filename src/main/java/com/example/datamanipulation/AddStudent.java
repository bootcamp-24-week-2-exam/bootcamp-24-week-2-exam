package com.example.datamanipulation;

import com.example.interfaces.AddStudentInterface;

import javax.swing.plaf.nimbus.State;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Scanner;

public class AddStudent implements AddStudentInterface {

    private Statement statement;

    public AddStudent(Statement statement) {
        this.statement = statement;
    }


    @Override
    public void InputCredentials() {
        HashMap<String, String> studenInfo = new HashMap<>();
        Scanner scanner  = new Scanner(System.in);
        System.out.println("< ======== Adding Student ======== >");
        System.out.print("1] First name: ");
        studenInfo.put("firstname", scanner.nextLine());
        System.out.print("2] Last name: ");
        studenInfo.put("lastname", scanner.nextLine());
        System.out.print("3] Middle name: ");
        studenInfo.put("middlename", scanner.nextLine());
        System.out.print("4] Address: ");
        studenInfo.put("address", scanner.nextLine());
        System.out.print("5] ZIP code: ");
        studenInfo.put("zipcode", scanner.nextLine());
        System.out.print("6] City: ");
        studenInfo.put("city", scanner.nextLine());
        System.out.print("7] Region/Province: ");
        studenInfo.put("regionprovince", scanner.nextLine());
        System.out.print("8] Telephone number: ");
        studenInfo.put("phonenumber", scanner.nextLine());
        System.out.print("9] Mobile number: ");
        studenInfo.put("mobilenumber", scanner.nextLine());
        System.out.print("10] Email Address: ");
        studenInfo.put("emailaddress", scanner.nextLine());
        System.out.print("11] Year Level: ");
        studenInfo.put("yearlevel", scanner.nextLine());

        // pass to save credentials.
        SaveCredentials(studenInfo);
    }

    @Override
    public void SaveCredentials(HashMap<String, String> studentInfo) {
        // make SQL Query
        String SQLquery = "";

        // initiate try and catch.
        try {
            this.statement.executeUpdate(SQLquery);
            String message = "Note: Student added successfully!";
            AddStudentStatus(message);
        } catch (SQLException e) {
            String message = "Error: " + e;
            AddStudentStatus(message);
        }
    }

    @Override
    public void AddStudentStatus(String message) {
        System.out.println(message);
    }
}
