package com.example.datamanipulation;

import com.example.interfaces.DeleteStudentInterface;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class DeleteStudent implements DeleteStudentInterface {

    private Statement statement;

    public DeleteStudent(Statement statement) {
        this.statement = statement;
    }

    @Override
    public void DeleteWhichStudent() {
        String studentEmail;
        Scanner scanner = new Scanner(System.in);
        System.out.println("< ================== Delete a Student ================ >");
        System.out.println("(Note: use the View Student Listing to view students first before you edit.)");
        System.out.println("Input student email address to delete (Press enter to go back)");
        System.out.print("Email: ");
        studentEmail = scanner.nextLine();

        if (studentEmail.isEmpty()) {
            System.out.println();
        } else {
            //pass value to EnterNewStudentInfo.
            DeletingStudent(studentEmail);
        }

    }

    @Override
    public void DeletingStudent(String studentEmail) {
            // find student first.

            //SQL query for search.
            String SQLquerygetStudent = "";
            String SQLquerydeleteStudent;

            // try catch
            try {
                // execute query and get resultset.
                ResultSet resultSet = this.statement.executeQuery(SQLquerygetStudent);

                Scanner scanner = new Scanner(System.in);
                // display to user which student he/she is deleting.
                System.out.println("Deleting Student " + resultSet.getString("firstname") + " " + resultSet.getString("lastname"));
                System.out.println("Are you sure? (y/n)");
                String choice = scanner.nextLine();

                if(choice == "y") {

                    //run sql query to delete.
                    SQLquerydeleteStudent = "";

                    try {
                        //delete student..
                        this.statement.executeUpdate(SQLquerydeleteStudent);
                        // deleted message.
                        String message = "Note: Student has been deleted.";
                        DeleteStudentStatus(message);
                    } catch (SQLException e) {
                        String message = "Error: " + e;
                    }

                } else if(choice == "n"){
                    // return to menu.
                    System.out.println();
                } else {
                    // return to menu.
                    System.out.println();
                }


            } catch (SQLException e) {
                String message = "Error: " + e;
                DeleteStudentStatus(message);
            }
    }

        @Override
        public void DeleteStudentStatus (String message){
            System.out.println(message);
        }

}
