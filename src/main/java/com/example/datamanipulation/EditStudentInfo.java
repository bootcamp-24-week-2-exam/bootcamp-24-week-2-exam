package com.example.datamanipulation;

import com.example.interfaces.EditStudentInfoInterface;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Scanner;

public class EditStudentInfo implements EditStudentInfoInterface {

    private Statement statement;

    public EditStudentInfo(Statement statement) {
        this.statement = statement;
    }
    @Override
    public void EditWhichStudent() {
        String studentSearch;
        Scanner scanner = new Scanner(System.in);
        // ask user which student he wants to edit.
        System.out.println("< ============ Edit Student Info =========== >");
        System.out.println("(Note: use the View Student Listing to view students first before you edit.)");
        System.out.println("Which student do you want to edit? (search by name or Enter to go back)");
        studentSearch = scanner.nextLine();
        if (studentSearch.isEmpty()) {
            System.out.println();
        } else {
            //pass value to EnterNewStudentInfo.
            EnterNewStudentInfo(studentSearch);
        }

    }

    @Override
    public void EnterNewStudentInfo(String studentSearch) {
        // find student first.

        //SQL query for search.
        String SQLquery = "";

        //try catch
        try {
            // execute query and get resultset.
            ResultSet resultSet = this.statement.executeQuery(SQLquery);
            // display to user which student he/she is editting.
            System.out.println("Editing Student " + resultSet.getString("firstname") + " " + resultSet.getString("lastname"));

            // user input.
            HashMap<String, String> studenInfo = new HashMap<>();
            Scanner scanner  = new Scanner(System.in);
            System.out.print("1] First name: ");
            studenInfo.put("firstname", scanner.nextLine());
            System.out.print("2] Last name: ");
            studenInfo.put("lastname", scanner.nextLine());
            System.out.print("3] Middle name: ");
            studenInfo.put("middlename", scanner.nextLine());
            System.out.print("4] Address: ");
            studenInfo.put("address", scanner.nextLine());
            System.out.print("5] ZIP code: ");
            studenInfo.put("zipcode", scanner.nextLine());
            System.out.print("6] City: ");
            studenInfo.put("city", scanner.nextLine());
            System.out.print("7] Region/Province: ");
            studenInfo.put("regionprovince", scanner.nextLine());
            System.out.print("8] Telephone number: ");
            studenInfo.put("phonenumber", scanner.nextLine());
            System.out.print("9] Mobile number: ");
            studenInfo.put("mobilenumber", scanner.nextLine());
            System.out.print("10] Email Address: ");
            studenInfo.put("emailaddress", scanner.nextLine());
            System.out.print("11] Year Level: ");
            studenInfo.put("yearlevel", scanner.nextLine());
            System.out.print("Are you sure with these changes? (y/n): ");
            String choice = scanner.nextLine();

            if (choice == "y") {
                // pass value to updatestudentinfo.
                UpdateStudentInfo(studenInfo);
            } else if(choice == "n") {
                // return to menu.
                System.out.println();
            } else {
                // return to menu.
                System.out.println();
            }



        } catch (SQLException e) {
            String message = "Error: " + e;
            EditStudentInfoStatus(message);
        }

    }

    @Override
    public void UpdateStudentInfo(HashMap<String, String> studentInfo) {
        try {
            // SQL query for update.
            String message;
            String SQLquery = "";
            this.statement.executeUpdate(SQLquery);

            // put status.
            message = "Note: Student has been edited.";
            EditStudentInfoStatus(message);
        } catch (SQLException e) {
            String message = "Error: " + e;
            EditStudentInfoStatus(message);
        }
    }

    @Override
    public void EditStudentInfoStatus(String message) {
        System.out.println(message);
    }
}
