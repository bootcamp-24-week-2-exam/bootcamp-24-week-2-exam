package com.example.dataoverview;

import com.example.interfaces.ViewStudentListingInterface;

import javax.swing.plaf.nimbus.State;
import java.sql.Statement;

public class ViewStudentListing implements ViewStudentListingInterface {

    private Statement statement;

    public ViewStudentListing(Statement statement) {
        this.statement = statement;
    }
    @Override
    public void SortedbyNameYearSection() {

    }

    @Override
    public void ListStudentsPerYear() {

    }

    @Override
    public void ListStudentsPerSection() {

    }

    @Override
    public void ListStudentsPerCity() {

    }

    @Override
    public void ListStudentsPerZip() {

    }

    @Override
    public void ListofStudentPerAgeBracket() {

    }
}
