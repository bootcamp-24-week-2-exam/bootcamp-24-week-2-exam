package com.example.interfaces;

import java.sql.Statement;

public interface ViewStudentListingInterface {
    public void SortedbyNameYearSection();
    public void ListStudentsPerYear();
    public void ListStudentsPerSection();
    public void ListStudentsPerCity();
    public  void ListStudentsPerZip();
    public void ListofStudentPerAgeBracket();
}
