package com.example.interfaces;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;

public interface EditStudentInfoInterface {

    public void EditWhichStudent();
    public void EnterNewStudentInfo(String studentSearch);
    public void UpdateStudentInfo(HashMap<String, String> studentInfo);
    public void EditStudentInfoStatus(String message);
}
