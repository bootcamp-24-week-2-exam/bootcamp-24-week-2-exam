package com.example.interfaces;

import java.sql.Statement;
import java.util.HashMap;

public interface AddStudentInterface {

    public void InputCredentials();
    public void SaveCredentials(HashMap<String, String> studentInfo);
    public void AddStudentStatus(String message);
}
