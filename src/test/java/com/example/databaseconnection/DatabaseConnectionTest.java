package com.example.databaseconnection;

import io.cucumber.java.en.Given;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.function.BooleanSupplier;

import static org.junit.jupiter.api.Assertions.*;

class DatabaseConnectionTest {

    @Test
    void getConnectionTest() throws SQLException {
        String host = "localhost";
        String databaseName = "defaultdatabase";
        String username = "user";
        String password = "Password_12";
        DatabaseConnection databaseConnection = new DatabaseConnection(host, databaseName, username, password);

        // testing the connection to the database.
        try {
            Connection connection = databaseConnection.getConnection();
            Assertions.assertTrue(true);
        } catch (SQLException e) {
            Assertions.assertFalse(false);
        }

    }

}