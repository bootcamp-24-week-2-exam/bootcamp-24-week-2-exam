package com.example.datamanipulation;

import com.example.databaseconnection.DatabaseConnection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

class AddStudentTest {

    // connection info.
    Statement statement;
    @BeforeEach
    void AddStudentTest() {
        String host = "localhost";
        String databaseName = "defaultdatabase";
        String username = "user";
        String password = "Password_123";


        //check connection.
        DatabaseConnection databaseConnection = new DatabaseConnection(host, databaseName, username, password);

        // make a statement for accepting sql queries.
        try {
            // make connection.
            Connection connection = databaseConnection.getConnection();
            this.statement = connection.createStatement();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    @Test
    public void SaveCredentiaTest() {
        //input test.
        HashMap<String, String> studenInfo = new HashMap<>();
        studenInfo.put("firstname", "Cardo");
        studenInfo.put("lastnname", "Dalisay");
        studenInfo.put("middlename", "Probinsiyano");
        studenInfo.put("address", "6801 Ayala avenue");
        studenInfo.put("zipcode", "2000");
        studenInfo.put("city", "Makati");
        studenInfo.put("regionprovince", "Metro Manila Region");
        studenInfo.put("phonenumber", "+2000000");
        studenInfo.put("mobilenumber", "09000000000");
        studenInfo.put("emailaddress", "cardodalisay@gmail.com");
        studenInfo.put("yearlevel", "2");


        // make SQL Query
        String SQLquery = "";

        //check if query is empty.
        if (SQLquery.isEmpty()) {
            Assertions.assertFalse(false);
        } else {
            Assertions.assertTrue(true);
        }

        // initiate try and catch.
        try {
            // execute queries.
            this.statement.executeUpdate(SQLquery);
            String message = "Note: Student added successfully!";
            Assertions.assertTrue(true);
        } catch (SQLException e) {
            String message = "Error: " + e;
            Assertions.assertFalse(false);
        }

    }

}