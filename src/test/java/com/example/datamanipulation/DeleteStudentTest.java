package com.example.datamanipulation;

import com.example.databaseconnection.DatabaseConnection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


class DeleteStudentTest {


    Statement statement;

    @BeforeEach
    void DeleteStudentTest() {
        // connection info.
        String host = "localhost";
        String databaseName = "defaultdatabase";
        String username = "user";
        String password = "Password_123";

        //check connection.
        DatabaseConnection databaseConnection = new DatabaseConnection(host, databaseName, username, password);
        // make connection.
        try {
            Connection connection = databaseConnection.getConnection();
            // make a statement for accepting sql queries.
            this.statement = connection.createStatement();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }


    @Test
    void DeletingStudentTest() {
        // student email input sample.
        String studentEmail = "";

        //SQL query for search.
        String SQLquerygetStudent = "";
        String SQLquerydeleteStudent;

        // try catch
        try {
            // execute query and get resultset.
            ResultSet resultSet = this.statement.executeQuery(SQLquerygetStudent);
            //connections to the database succeed.
            Assertions.assertTrue(true);

        } catch(SQLException e) {
            //connection to the database failed.
            Assertions.assertFalse(false);
        }

        //run sql query to delete.
        SQLquerydeleteStudent = "";

        try {
            //delete student..
            this.statement.executeUpdate(SQLquerydeleteStudent);
            // success in deleting the student..
            Assertions.assertTrue(true);
        } catch (SQLException e) {
            // failed to execute SQL query.
           Assertions.assertFalse(false);
        }
    }
}