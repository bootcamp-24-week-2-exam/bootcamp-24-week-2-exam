package com.example.datamanipulation;

import com.example.databaseconnection.DatabaseConnection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

class EditStudentInfoTest {
    // connection info.
    Statement statement;

    @BeforeEach
    void EditStudentInfoStatus(){
        String host = "localhost";
        String databaseName = "defaultdatabase";
        String username = "user";
        String password = "Password_123";
        //check connection.
        DatabaseConnection databaseConnection = new DatabaseConnection(host, databaseName, username, password);
        // make connection.
        Connection connection = null;
        try {
            connection = databaseConnection.getConnection();
            // make a statement for accepting sql queries.
            this.statement = connection.createStatement();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Test
    void EnterNewStudentInfo(){

        //SQL query for search.
        String SQLquery = "";

        //check if query is empty.
        if (SQLquery.isEmpty()) {
            Assertions.assertFalse(false);
        } else {
            Assertions.assertTrue(true);
        }

        //try catch
        try {
            ResultSet resultSet = this.statement.executeQuery(SQLquery);
            // display to user which student he/she is editting.
            System.out.println("Editing Student " + resultSet.getString("firstname") + " " + resultSet.getString("lastname"));
            // assert if database connection is successful.
            Assertions.assertTrue(true);
        } catch (SQLException e) {
            String message = "Error: " + e;
            // assert if database connection fails.
            Assertions.assertFalse(false);
        }
    }

    @Test
    void UpdateStudentInfo() {
        try {
            // SQL query for update.
            String message;
            String SQLquery = "";
            this.statement.executeUpdate(SQLquery);
            Assertions.assertTrue(true);

        } catch (SQLException e) {
            Assertions.assertFalse(false);
        }
    }

}